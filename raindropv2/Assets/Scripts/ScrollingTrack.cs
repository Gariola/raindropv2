﻿using UnityEngine;
using System.Collections;

public class ScrollingTrack : MonoBehaviour
{
    public float speed = -0.05f;
    public Material[] cam;
    public float changeTime;
    float timer;
    float dummyTime = 0;
    int i;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        dummyTime += Time.deltaTime;
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = changeTime;
            dummyTime = -1.5f;
            i = Random.Range(1, 6);
            GetComponent<Renderer>().material = cam[i];
        }
        Vector2 offset = new Vector2(0, dummyTime * speed);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
