﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour
{
    public float cameraDistOffsetZ = 10;
    public float cameraDistOffsetY = 10;

    private Camera mainCamera;
    private GameObject player;
    float clampedX;

    float maxCameraFollow = 0;

    // Use this for initialization
    void Start()
    {
        mainCamera = GetComponent<Camera>();
        player = GameObject.Find("Dropli");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerInfo = player.transform.transform.position;
        clampedX = Mathf.Clamp(playerInfo.x, -maxCameraFollow, maxCameraFollow);
        mainCamera.transform.position = new Vector3(clampedX, playerInfo.y - cameraDistOffsetY, playerInfo.z - cameraDistOffsetZ);
    }
}

