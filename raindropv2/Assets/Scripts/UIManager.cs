﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public dropliControl dc;
    public int iceDelay;
    float timer = 0;
    public int water ;
    int iceCube = 0;
    public GameObject[] iceCubeUI;
    int i;
    int j;
    public AudioSource[] sounds;
    public AudioSource bubblePop;
    public AudioSource iceCrush;
    public AudioSource dropCollect;

    public Button play;
    public Button replay;
    public Button backToMenu;
    // Use this for initialization
    void Start () {
        Time.timeScale = 0;
        sounds = GetComponents<AudioSource>();
        bubblePop = sounds[0];
        iceCrush = sounds[1];
        dropCollect = sounds[2];
    }
	
	// Update is called once per frame
	void Update () {


        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            water--;
            timer = 1.0f;
        }
        if (water <= 0)
        {
            GameOver();
        }

        //iceCubeUI code 
        j = 0;
        for(i=0; i<iceCube;i++)
        {
            iceCubeUI[i].SetActive(true);
            j++;
        }
        for (i=j; i < 5; i++)
        {
            iceCubeUI[i].SetActive(false);
        }
        Debug.Log(Time.time);
    }

    public void GameOver() {
        Time.timeScale = 0;
        replay.gameObject.SetActive(true);
        backToMenu.gameObject.SetActive(true);
    }

    public void Refill() {
        dropCollect.Play();
        water += 1;
        if (water > 20){
            water = 20;
            timer = 1.0f;
        }
    }
    public void RefillIce() {
        if (iceCube > 0)
        {
            water += 2;
            iceCube--;
            if (water > 20)
                water = 20;
            timer = 1f;
        }
        
    }

    public void DelayedRefill(){
        iceCube++;
        if (iceCube > 5) iceCube = 5;
        /*Invoke("RefillIce", iceDelay);
        iceCrush.PlayDelayed(3f);*/
    }

    public void Bubble()
    {
        bubblePop.Play();
        dc.TouchedBubble();
    }

    public void Mud()
    {
        dc.TouchedMud();
    }

    public void Play()
    {
        Time.timeScale = 1;
        play.gameObject.SetActive(false);
    }

    public void Replay()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void BackToMenu()
    {
        Application.LoadLevel(0);
    }
}
