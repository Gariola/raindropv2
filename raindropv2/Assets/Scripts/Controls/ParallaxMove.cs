﻿using UnityEngine;
using System.Collections;

public class ParallaxMove : MonoBehaviour {
    public float clusterSpeed;

	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {

        transform.Translate(new Vector3(0, -1, 0) * clusterSpeed * Time.deltaTime * -1);

    }
}
