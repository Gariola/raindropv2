﻿using UnityEngine;
using System.Collections;

public class BugController : MonoBehaviour {

    public UIManager ui;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D cols)
    {
        if (cols.gameObject.tag == "Player")
        {
            ui.GameOver();
        }
    }
}
