﻿using UnityEngine;
using System.Collections;

public class ButterflyControl: MonoBehaviour
{
    public float butterflySpeed;
    public UIManager ui;
    public float direction;
    Vector3 theScale;
    float timer=0;
    bool turnReady = true;

    private int currentColliderIndex = 0;
    [SerializeField]
    private PolygonCollider2D[] colliders;

    public void SetColliderForSprite(int spriteNum)
    {
        //Debug.Log(currentColliderIndex);
        colliders[currentColliderIndex].enabled = false;
        currentColliderIndex = spriteNum;
        colliders[currentColliderIndex].enabled = true;
    }
    // Use this for initialization
    void Start()
    {
        theScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer<0)
        {
            turnReady = true;
        }
        transform.Translate(new Vector3(-direction, 0, 0) * butterflySpeed * Time.deltaTime * -1);

    }

   
    void OnCollisionEnter2D(Collision2D cols)
    {
        if (cols.gameObject.tag == "wall" && turnReady)
        {
            //Debug.Log("triggered");
            direction = direction * -1;
            theScale.x *= -1;
            transform.localScale = theScale;

            turnReady = false;
            timer = 0.5f;
        }
        if (cols.gameObject.tag == "Player")
        {
            ui.GameOver();
        }
    }
}
