﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class dropliControl : MonoBehaviour {
    public Rigidbody2D myRigidbody2D { get; private set; }
    float inertia;
    float currentVelocity;
    public float dropliSpeed;
    Vector2 vel;
    float initialDrag;
    float initialMass;
    int bubbleStatus = 0;
    int mudStatus = 0;
    float bubbleTimer;
    float mudTimer;
    public float dropliFallSpeed;
    float snowTimer = 0;
    float snowStatus = 0;
    float snowDoubleDummyTimer = 0;
    float androidInertia=0;
    float snowCoeff=1f;
    float bubbleCoeff=1f;
    public Button replay;
    public Button backToMenu;


    // Use this for initialization
    void Start () {
        this.myRigidbody2D = this.GetComponent<Rigidbody2D>();
        initialMass = myRigidbody2D.mass;
        initialDrag = myRigidbody2D.drag;
    }
	
	// Update is called once per frame
	void Update () {
        bubbleTimer -= Time.deltaTime;
        if (bubbleTimer < 0) bubbleStatus = 0;

        bubbleCoeff = 1 + bubbleStatus;
        mudTimer -= Time.deltaTime;
        if (mudTimer < 0) mudStatus = 0;

        snowTimer -= Time.deltaTime;
        if (snowTimer < 0) snowTimer = 0;

        if(snowDoubleDummyTimer>0)
        {
            snowDoubleDummyTimer -= Time.deltaTime;
        }

        if(snowDoubleDummyTimer<0)
        {
            snowDoubleDummyTimer = 0;
        }
        snowStatus = Mathf.Floor(snowDoubleDummyTimer + 0.8f);
        snowCoeff = 1 - 0.15f * snowStatus;

#if UNITY_EDITOR

        if (inertia != Input.GetAxis("Horizontal"))
        {
            currentVelocity = myRigidbody2D.velocity.x;
            myRigidbody2D.velocity = new Vector2(currentVelocity * 0.75f, 0);
            inertia = Input.GetAxis("Horizontal");
        }
        myRigidbody2D.AddForce(Vector2.right * Input.GetAxis("Horizontal") * dropliSpeed, ForceMode2D.Force);
#endif
#if UNITY_ANDROID


        vel.x = Input.acceleration.x * dropliSpeed * snowCoeff * bubbleCoeff;
        vel.y = -dropliFallSpeed;

        myRigidbody2D.velocity = new Vector2(vel.x, 0);
#endif

        //myRigidbody2D.mass = initialMass + -0.1f * bubbleStatus + 0.6f * mudStatus + 0.1f * snowStatus;
        //myRigidbody2D.drag = initialDrag + -0.7f * bubbleStatus;



       transform.Translate(new Vector3(0, -1, 0) * dropliFallSpeed * Time.deltaTime );

    }



    public void TouchedBubble()
    {
        bubbleTimer = 5f;
        bubbleStatus = 1;
    }

    public void TouchedMud()
    {
        mudTimer = 5f;
        mudStatus = 1;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.gameObject.tag == "Snow")
        {
            snowTimer += 2 * Time.deltaTime;
            snowDoubleDummyTimer = Mathf.Clamp(snowTimer, 0f, 5f);

        }
        if (col.gameObject.tag == "end")
        {
            Time.timeScale = 0;
            replay.gameObject.SetActive(true);
            backToMenu.gameObject.SetActive(true);
        }
    }
}
