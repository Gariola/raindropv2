﻿using UnityEngine;
using System.Collections;

public class BeeController : MonoBehaviour {

    public float angle;
    public float beeSpeed;
    public float beeRotateSpeed;
    private Vector3 center;
    public float radius;


    public UIManager ui;

	// Use this for initialization
	void Start () {
        radius = 1f;
        center.x = transform.position.x;
        center.z = transform.position.z;
        center.y = transform.position.y - radius;
       
	
	}
	
	// Update is called once per frame
	void Update () {


        //transform.Translate(new Vector3(0, -1, 0) * beeSpeed * Time.deltaTime * -1, Space.World);
        //center.y += beeSpeed * Time.deltaTime ;
        
        transform.RotateAround(center, Vector3.back, angle*Time.deltaTime);
       // transform.Translate(new Vector3(0, -1, 0) * beeRotateSpeed * Time.deltaTime * -1, Space.Self);
      

    }

    void OnTriggerStay2D (Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            ui.GameOver();
        }
    }
}
