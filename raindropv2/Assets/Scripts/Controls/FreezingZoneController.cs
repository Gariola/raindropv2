﻿using UnityEngine;
using System.Collections;

public class FreezingZoneController : MonoBehaviour {
    public float freezingSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(new Vector3(0, -1, 0) * freezingSpeed * Time.deltaTime * -1);
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            gameObject.GetComponent<Animator>().SetFloat("inside",1);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            gameObject.GetComponent<Animator>().SetFloat("inside", 0);
        }
    }
}
