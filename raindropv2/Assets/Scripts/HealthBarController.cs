﻿using UnityEngine;
using System.Collections;

public class HealthBarController : MonoBehaviour {
    public UIManager ui;
    public Sprite[] healthBar;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<SpriteRenderer>().sprite = healthBar[ui.water];
    }
}
